import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';

import { TravelMatrixModel, TravelTime } from './travel-matrix/travel-matrix.interface';
import { IncidentPointModel } from './incident-point/incident-point.interface';
import { TravelPointModel } from './travel-point/travel-point.interface';
import { MindConnectApiService } from './mindconnect.service';

@Injectable()
export class AppService {
  constructor(
    private readonly _mcService: MindConnectApiService,
    @InjectModel('TravelPoint') private readonly travelPointModel: Model<TravelPointModel>,
    @InjectModel('IncidentPoint') private readonly incidentPointModel: Model<IncidentPointModel>,
    @InjectModel('TravelMatrix') private readonly travelMatrixModel: Model<TravelMatrixModel>,
  ) {

  }
  getHello(): string {
    return 'Hello World!';
  }

  async getData() {
    const tPoints = await this.travelPointModel.find({}).lean().exec();
    const iPoints = await this.incidentPointModel.find({}).lean().exec();
    const vehicles = await this.travelMatrixModel.findOne().sort({_id: -1}).limit(1).exec();
    const { vehiclesData } = vehicles;

    return {
      travelPoints: tPoints.map((t) => ({
        ...t, 
        vehicles: this._getTravelPointMinTimeValueVehicles(t._id, vehiclesData),
        times: this._getTravelPointMinTimeValue(t._id, vehiclesData),
      })),
      incidentPoints: iPoints.map((i) => ({
        ...i,
        vehicles: this._getIncidentPointMinTimeValueVehicles(i._id, vehiclesData),
        times: this._getIncidentPointMinTimeValue(i._id, vehiclesData)
      })),
      vehicles: vehiclesData.map((v) => ({id: v.id, name: v.name, position: v.position}))
    }
  }

  private _getTravelPointMinTimeValueVehicles(id: string, vehicles: any[]): number[] {
    const vehiclesToSearch = vehicles.map((v) => ({...v, time: v.travelTimes.find((t) => t.entityId.toString() === id.toString()).value}))
    vehiclesToSearch.sort((a, b) => a.time - b.time);
    return vehiclesToSearch.filter((val) => val.time > 0).slice(0, 1);
  }

  private _getIncidentPointMinTimeValueVehicles(id: string, vehicles: any[]): number[] {
    const vehiclesToSearch = vehicles.map((v) => ({...v, time: v.travelTimes.find((t) => t.entityId.toString() === id.toString()).value}))
    vehiclesToSearch.sort((a, b) => a.time - b.time);
    return vehiclesToSearch.filter((val) => val.time > 0).slice(0, 3);
  }

  private _getTravelPointMinTimeValue(id: string, vehicles: any[]): number[] {
    const vehiclesToSearch = vehicles.map((v) => v.travelTimes.find((t) => t.entityId.toString() === id.toString()).value)
    vehiclesToSearch.sort((a, b) => a - b);
    return vehiclesToSearch.filter((val) => val > 0).slice(0, 1);
  }

  private _getIncidentPointMinTimeValue(id: string, vehicles: any[]): number[] {
    const vehiclesToSearch = vehicles.map((v) => v.travelTimes.find((t) => t.entityId.toString() === id.toString()).value)
    vehiclesToSearch.sort((a, b) => a - b);
    return vehiclesToSearch.filter((val) => val > 0).slice(0, 3);
  }
  @Cron('0 */5 * * * *')
  async getTravelPointFastestRoutes() {
    try {
      const vehicles = await this._mcService.getVehicles().toPromise();
    
      const travelPoints: TravelPointModel[] = await this.travelPointModel.find({}).exec();
      const incidentPoints: IncidentPointModel[] = await this.incidentPointModel.find({}).exec();
  
      const matrix: number[][] = await this._mcService.generateTimeMatrix([
        ...travelPoints.map((t) => t.position),
        ...incidentPoints.map((i) => i.position),
        ...vehicles.map((v) => v.position)
      ]).toPromise();
      
      const entityIds = [...travelPoints.map((t) => t._id), ...incidentPoints.map((i) => i._id)]
      
      const vehiclesToStore = vehicles.map((v, index: number) => ({
          ...v,
          travelTimes: this.getVehicleTravelTimes(entityIds, matrix[index + entityIds.length])
      }))
  
      const travelMatrix = new this.travelMatrixModel({vehiclesData: vehiclesToStore});
      
      await travelMatrix.save();
    } catch (err) {
      console.log(err);
    }
  }
  @Cron('40 */1 * * * *')
  async getVehiclesPosition() {
    try {
      const vehicles = await this._mcService.getVehicles().toPromise();
      const travelMatrix = await this.travelMatrixModel.findOne().sort({_id: -1}).limit(1).lean().exec();

      const dataToUpdate = travelMatrix.vehiclesData.map((vehicle) => {
        const existVehicle = vehicles.find((v) => v.id === vehicle.id);
        if (existVehicle) {
          return {
            ...vehicle,
            position: existVehicle.position
          }
        } else {
          return vehicle
        }
      });

      await this.travelMatrixModel.findByIdAndUpdate(travelMatrix._id, {$set: {vehiclesData: dataToUpdate}}).exec();
    } catch (err) {
      console.log(err);
    }
  }

  private getVehicleTravelTimes(entityIds: string[], timeMatrix: number[]): TravelTime[] {
    return entityIds.map((id: string, index: number) => ({
      entityId: id,
      value: timeMatrix[index]
    }))
  }
}
