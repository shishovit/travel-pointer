import { MindConnectApiService } from './../mindconnect.service';
import { TravelMatrixService } from './../travel-matrix/travel-matrix.service';
import { TravelMatrixSchema } from './../travel-matrix/travel-matrix.schema';
import { IncidentPointSchema } from './incident-point.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { Module, HttpModule } from '@nestjs/common';
import { IncidentPointService } from './incident-point.service';
import { IncidentPointController } from './incident-point.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{name: 'IncidentPoint', schema: IncidentPointSchema}]),
    MongooseModule.forFeature([{name: 'TravelMatrix', schema: TravelMatrixSchema}]),
    HttpModule
  ],
  providers: [IncidentPointService, TravelMatrixService, MindConnectApiService],
  controllers: [IncidentPointController]
})
export class IncidentPointModule {}
