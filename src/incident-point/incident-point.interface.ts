import { Document } from 'mongoose';

export interface Coordnates {
  lat: number;
  lng: number;
}

export interface IncidentPoint {
  title: string;
  position: Coordnates;
  direction: number;
}

export interface IncidentPointModel extends IncidentPoint, Document {};

