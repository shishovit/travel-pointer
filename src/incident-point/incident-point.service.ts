import { TravelTime } from './../travel-matrix/travel-matrix.interface';
import { MindConnectApiService } from './../mindconnect.service';
import { TravelMatrixService } from './../travel-matrix/travel-matrix.service';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Injectable } from '@nestjs/common';
import { IncidentPoint, IncidentPointModel } from './incident-point.interface';
@Injectable()
export class IncidentPointService {
  constructor(@InjectModel('IncidentPoint') private readonly incidentPointModel: Model<IncidentPointModel>,
    private readonly _tmService: TravelMatrixService,
    private readonly _msService: MindConnectApiService) { }

  async create(pointData: IncidentPoint): Promise<any> {
    
    const vehicles = await this._tmService.findLast();
    
    const { vehiclesData } = vehicles;
    
    const matrix = await this._msService.generateTimeMatrix([
      pointData.position,
      ...vehiclesData.map((v) => v.position)
    ]).toPromise();
    
    const point = new this.incidentPointModel(pointData);
    await point.save();

    const entityIds = [point._id.toString()]

    const vehiclesToStore = vehiclesData.map((v, index: number) => ({
      ...v,
      travelTimes: [...v.travelTimes, ...this.getVehicleTravelTimes(entityIds, matrix[index + entityIds.length])]
    }))

    const vehicleMatrix = await this._tmService.updateMatrix(vehicles['_id'].toString(), {vehiclesData: vehiclesToStore})

    return {
      ...point.toObject(), 
      vehicles: this._getIncidentPointMinTimeValueVehicles(point._id, vehicleMatrix.vehiclesData),
      times: this._getIncidentPointMinTimeValue(point._id, vehicleMatrix.vehiclesData)
    };
  }

  async findAll(): Promise<IncidentPoint[]> {
    return await this.incidentPointModel.find().select('-__v').exec();
  }

  async remove(pointId: string): Promise<{ status: boolean }> {
    await this.incidentPointModel.findByIdAndRemove(pointId).exec();
    return { status: true };
  }

  async getIncidentPointRoutes(id: string) {
    const incidentPoint = await this.incidentPointModel.findById(id).exec();
    const vehicles = await this._tmService.findLast();

    const { vehiclesData } = vehicles;
    const vehiclesToSearch = vehiclesData.map((v) => ({ 
      id: v.id, 
      name: v.name, 
      position: v.position, 
      travelTime: v.travelTimes.find((t) => t.entityId.toString() === id.toString()).value 
    }))
    
    vehiclesToSearch.sort((a, b) => a.travelTime - b.travelTime);
    const threeVehicles = vehiclesToSearch.filter((val) => val.travelTime > 0).slice(0, 3);
    
    const result = await Promise.all(threeVehicles.map((v) => this._msService.getClosestVehicle({
      location: { lat: incidentPoint.position.lat, lng: incidentPoint.position.lng, dir: incidentPoint.direction },
      vehicles: [{ 'vehicle_type': 2, location: { lat: v.position.lat, lng: v.position.lng } }]
    }).toPromise()))

    return threeVehicles.map((v, i) => ({ ...v, polyline: result[i].polyline }));
  }

  private getVehicleTravelTimes(entityIds: string[], timeMatrix: number[]): TravelTime[] {
    return entityIds.map((id: string, index: number) => ({
      entityId: id,
      value: timeMatrix[index]
    }))
  }

  private _getIncidentPointMinTimeValue(id: string, vehicles: any[]): number[] {
    const vehiclesToSearch = vehicles.map((v) => v.travelTimes.find((t) => t.entityId.toString() === id.toString()).value)
    vehiclesToSearch.sort((a, b) => a - b);
    return vehiclesToSearch.slice(0, 3);
  }

  private _getIncidentPointMinTimeValueVehicles(id: string, vehicles: any[]): number[] {
    const vehiclesToSearch = vehicles.map((v) => ({...v, time: v.travelTimes.find((t) => t.entityId.toString() === id.toString()).value}))
    vehiclesToSearch.sort((a, b) => a.time - b.time);
    return vehiclesToSearch.filter((val) => val.time > 0).slice(0, 3);
  }
}