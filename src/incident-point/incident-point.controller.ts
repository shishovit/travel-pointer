import { HttpExceptionFilter } from './../error-guard';
import { Get, Post, Delete, Param, Body, Controller, UseFilters } from '@nestjs/common';
import { IncidentPoint } from './incident-point.interface';
import { IncidentPointService } from './incident-point.service'
@Controller('incident-point')
export class IncidentPointController {
  constructor(private readonly _ipService: IncidentPointService) {}

  @Get('')
  async getPoints(): Promise<IncidentPoint[]> {
    return await this._ipService.findAll();
  }
  
  @Post('')
  async addPoint(@Body() pointData: IncidentPoint): Promise<IncidentPoint> {
    return await this._ipService.create(pointData);
  }

  @Delete(':id')
  async removePoint(@Param('id') id: string): Promise<{status: boolean}> {
    return await this._ipService.remove(id);
  }

  @Get(':id/routes')
  @UseFilters(new HttpExceptionFilter())
  async getIncidentPointRoutes(@Param('id') id: string): Promise<any> {
    return await this._ipService.getIncidentPointRoutes(id);
  }
}
