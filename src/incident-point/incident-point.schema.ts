import * as mongoose from 'mongoose';

export const IncidentPointSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  position: {
    lat: Number,
    lng: Number
  },
  direction: Number,
});