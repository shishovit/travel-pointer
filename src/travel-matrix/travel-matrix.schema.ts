import { TravelMatrixModel } from './travel-matrix.interface';
import * as mongoose from 'mongoose';

export const TravelMatrixSchema = new mongoose.Schema({
  vehiclesData: [], 
}, {timestamps: true});

export default mongoose.model<TravelMatrixModel>('travelMatrix', TravelMatrixSchema);