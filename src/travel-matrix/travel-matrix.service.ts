import {Model} from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { TravelMatrixModel, TravelMatrix } from './travel-matrix.interface';

@Injectable()
export class TravelMatrixService {
  constructor(
    @InjectModel('TravelMatrix') private readonly travelMatrixModel: Model<TravelMatrixModel>
  ) {}

  async create(vehicles: TravelMatrix): Promise<TravelMatrix> {
    const travelMatrix = new this.travelMatrixModel(vehicles);
    return travelMatrix.save();
  }

  async findMatrix(id: string): Promise<TravelMatrix> {
    return await this.travelMatrixModel.findById(id).select('-__v').exec();
  }

  async updateMatrix(id:string, dataToUpdate: any): Promise<TravelMatrix> {
    return await this.travelMatrixModel.findByIdAndUpdate(id, dataToUpdate, {new: true}).exec();
  }

  async findLast(): Promise<TravelMatrix> {
    return await this.travelMatrixModel.findOne().sort({_id: -1}).limit(1).exec();
  }
}