import { Document } from 'mongoose';

export interface Coordinates {
  lat: number;
  lng: number;
}

export interface TravelTime {
  entityId: string;
  value: number;
}

export interface TravelMatrix {
  vehiclesData: any[],
}

export interface TravelMatrixModel extends TravelMatrix, Document {};