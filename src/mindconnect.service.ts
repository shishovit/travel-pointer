import { AxiosResponse } from 'axios';
import { HttpService, Injectable } from '@nestjs/common';
import { map, tap, take, switchMap, retryWhen, delayWhen } from 'rxjs/operators';
import { Observable, timer } from 'rxjs';
export interface LoginResponse {
  id: number;
  username: string;
  token: string;
}

export interface Location {
  lat: number;
  lng: number;
  dir?: number;
}

export interface VehicleData {
  vehicle_type: number;
  location: Location
}

export interface ClosestVehiclePayload {
  location: Location;
  vehicles: VehicleData[];
}

export interface ClosestVehicleResponse {
  closest_vehicle: number; //vehicle ID
  polyline: [number, number][],
  cost: number; //time in milliseconds
}

export interface Vehicle {
  id: number;
  name: string;
  position: {
    latitude: number;
    longitude: number;
  }
}

export interface FormattedVehicle {
  id: number;
  name: string;
  position: Location;
}

export interface VehicleResponse {
  items: number;
  vehicles: Vehicle[];
}
@Injectable()
export class MindConnectApiService {
  private readonly baseURL = 'https://cloud.mindconnect.se/v1';
  private readonly username = 'Trafikverket';
  private readonly password = 'gecWeyGrysitar3';
  private userId = null;
  private token = null;
  private readonly latBounds = { 
    min: 59.084502, 
    max: 59.612623
   }
  private readonly lngBounds = {
    min: 17.546663, 
    max: 18.588123
  } 
  constructor(
    private readonly _httpService: HttpService
  ) {
    this.login().pipe(
      take(1),
      tap((res: LoginResponse) => {
        this.userId = res.id;
        this.token = res.token;
      }))
      .subscribe();
  }

  login(): Observable<LoginResponse> {
    return this._httpService
      .get(`${this.baseURL}/login?username=${this.username}&password=${this.password}`)
      .pipe(map((res: AxiosResponse) => res.data))
  }

  getClosestVehicle(payload: ClosestVehiclePayload): Observable<ClosestVehicleResponse> {
    return this._httpService
      .put(`${this.baseURL}/routing/closest_vehicle?user_id=${this.userId}&token=${this.token}&clipping_kind = true`, payload)
      .pipe(map((res: AxiosResponse) => res.data))
  }

  getVehicles(): Observable<FormattedVehicle[]> {
    return this._httpService
      .get('http://api.bmsystem.se/mobiwinqa/sverige/v1/vehicles?apiKey=6C0518196ADF425FAD9FBFD4B5B01E39')
      .pipe(
        map((res: AxiosResponse<VehicleResponse>) => res.data.vehicles
        .map((v) =>
          ({ id: v.id, name: v.name, position: { lat: parseFloat(v.position.latitude.toFixed(5)), lng: parseFloat(v.position.longitude.toFixed(5)) } } as FormattedVehicle))
        .filter((v) => this.compareLocation(v.position.lat, v.position.lng))
        )
      )
  }

  generateTimeMatrix(locations: Location[]) {
    return this._httpService
      .post(`${this.baseURL}/routing/matrix?user_id=${this.userId}&token=${this.token}`, {
        'vehicle_type': 2,
        'mode': 'fastest',
        locations
      })
      .pipe(
        map((res: AxiosResponse) => res.data.id),
        switchMap((id: number) => this._httpService.get(`${this.baseURL}/routing/matrix?user_id=${this.userId}&token=${this.token}&id=${id}`)
        .pipe(
            map(mres => {
              if (mres.data.progress !== void 0) {
                throw mres.data.progress;
              } else {
                return mres.data;
              }
            }),
            retryWhen(errors => errors.pipe(delayWhen(() => timer(2000))))
          )
        )
      )
  }

  private compareLocation(lat: number, lng: number): boolean {
    return (Math.round(lat * 100000) >= Math.round(this.latBounds.min * 100000))
      && (Math.round(lat * 100000) <= Math.round(this.latBounds.max * 100000)) 
      && (Math.round(lng * 100000) >= Math.round(this.lngBounds.min * 100000))
      && (Math.round(lng * 100000) >= Math.round(this.lngBounds.min * 100000));
  }
}
