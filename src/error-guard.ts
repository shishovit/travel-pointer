import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { Error } from 'mongoose';
import { AxiosError} from 'axios';
@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    // default HTTP response code for non HttpException errors is
    // 500 - Internal Server Error
    let status = 500;
    let errorMsg = 'Unrecognized error!';
    let externalURL = null;
    if (exception instanceof HttpException) {
      status = exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
    }
    
    if ((exception as AxiosError).isAxiosError) {
      status = (exception as AxiosError).response.status;
      errorMsg = (exception as AxiosError).response.statusText;
      externalURL = (exception as AxiosError).config.url
    }

    if (exception instanceof Error.ValidationError) {
      status = HttpStatus.BAD_REQUEST
      errorMsg = exception.message
    }

    response.status(status).json({
      statusCode: status,
      errorMsg,
      externalURL,
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}