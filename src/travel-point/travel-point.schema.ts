import * as mongoose from 'mongoose';
import { TravelPointModel } from './travel-point.interface';
export const TravelPointSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  position: {
    lat: Number,
    lng: Number
  },
  direction: Number,
  groupName: {
    type: String,
    required: true,
    default: 'Defaul Group'
  }
});

export default mongoose.model<TravelPointModel>('travelPoint', TravelPointSchema)