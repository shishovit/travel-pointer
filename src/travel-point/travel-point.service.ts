import { TravelTime } from './../travel-matrix/travel-matrix.interface';
import { TravelMatrixService } from './../travel-matrix/travel-matrix.service';
import { MindConnectApiService } from './../mindconnect.service';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { TravelPointModel, TravelPoint } from './travel-point.interface'; 
@Injectable()
export class TravelPointService {
  constructor(
    @InjectModel('TravelPoint') private readonly travelPointModel: Model<TravelPointModel>,
    private readonly _tmService: TravelMatrixService,
    private readonly _msService: MindConnectApiService) {}

  async create(pointData: TravelPoint): Promise<any> {
    const vehicles = await this._tmService.findLast();
    
    const { vehiclesData } = vehicles;
    
    const matrix = await this._msService.generateTimeMatrix([
      pointData.position,
      ...vehiclesData.map((v) => v.position)
    ]).toPromise();
    
    const point = new this.travelPointModel(pointData);
    await point.save();
    
    const entityIds = [point._id.toString()]

    const vehiclesToStore = vehiclesData.map((v, index: number) => ({
      ...v,
      travelTimes: [...v.travelTimes, ...this.getVehicleTravelTimes(entityIds, matrix[index + entityIds.length])]
    }))

    const vehicleMatrix = await this._tmService.updateMatrix(vehicles['_id'].toString(), {vehiclesData: vehiclesToStore})

    return {...point.toObject(), times: this._getTravelPointMinTimeValue(point._id, vehicleMatrix.vehiclesData)};
  }

  async findAll(): Promise<TravelPoint[]> {
    return await this.travelPointModel.find().select('-__v').exec();
  }

  async remove(pointId: string): Promise<{status: boolean}> {
    await this.travelPointModel.findByIdAndRemove(pointId).exec();
    return {status: true};
  }

  async removeByGroupName(groupName: string): Promise<{status: boolean}> {
    await this.travelPointModel.remove({groupName}).exec();
    return {status: true};
  }

  async getTravelPointRoutes(id: string) {
    const travelPoint = await this.travelPointModel.findById(id).exec();
    const vehicles = await this._tmService.findLast();
    
    const { vehiclesData } = vehicles;
    
    const vehiclesToSearch = vehiclesData.map((v) => ({
      id: v.id, 
      name: v.name, 
      position: v.position, 
      travelTime: v.travelTimes.find((t) => t.entityId.toString() === id.toString()).value}))
    
    vehiclesToSearch.sort((a, b) => a.travelTime - b.travelTime);
    
    const threeVehicles = vehiclesToSearch.filter((val) => val.travelTime > 0).slice(0, 3);
    
    const result = await Promise.all(threeVehicles.map((v) => this._msService.getClosestVehicle({
      location: {lat: travelPoint.position.lat, lng: travelPoint.position.lng, dir: travelPoint.direction},
      vehicles: [{'vehicle_type': 2, location: {lat: v.position.lat, lng: v.position.lng}}]
    }).toPromise()))
    
    return threeVehicles.map((v, i) => ({...v, polyline: result[i].polyline}));
  }

  private getVehicleTravelTimes(entityIds: string[], timeMatrix: number[]): TravelTime[] {
    return entityIds.map((id: string, index: number) => ({
      entityId: id,
      value: timeMatrix[index]
    }))
  }

  private _getTravelPointMinTimeValue(id: string, vehicles: any[]): number[] {
    const vehiclesToSearch = vehicles.map((v) => v.travelTimes.find((t) => t.entityId.toString() === id.toString()).value)
    vehiclesToSearch.sort((a, b) => a - b);
    return vehiclesToSearch.slice(0, 1);
  }
}
