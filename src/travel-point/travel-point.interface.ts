import { Document } from 'mongoose';

export interface Coordnates {
  lat: number;
  lng: number;
}

export interface TravelPoint {
  title: string;
  position: Coordnates;
  direction: number;
  groupName: string;
}

export interface TravelPointModel extends TravelPoint, Document {};
