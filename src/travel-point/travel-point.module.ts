import { TravelMatrixSchema } from './../travel-matrix/travel-matrix.schema';
import { MindConnectApiService } from './../mindconnect.service';
import { TravelMatrixService } from './../travel-matrix/travel-matrix.service';
import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TravelPointService } from './travel-point.service';
import { TravelPointController } from './travel-point.controller';
import { TravelPointSchema } from './travel-point.schema';
@Module({
  imports: [
    MongooseModule.forFeature([{name: 'TravelPoint', schema: TravelPointSchema}]),
    MongooseModule.forFeature([{name: 'TravelMatrix', schema: TravelMatrixSchema}]),
    HttpModule
  ],
  providers: [TravelPointService, TravelMatrixService, MindConnectApiService],
  controllers: [TravelPointController]
})
export class TravelPointModule {}
