import { HttpExceptionFilter } from './../error-guard';
import { TravelPoint } from './travel-point.interface';
import { TravelPointService } from './travel-point.service';
import { Get, Post, Delete, Param, Body, Controller, UseFilters } from '@nestjs/common';

@Controller('travel-point')
export class TravelPointController {
  
  constructor(private readonly _tpService: TravelPointService) {}

  @Get('')
  async getPoints(): Promise<TravelPoint[]> {
    return await this._tpService.findAll();
  }
  
  @Post('')
  async addPoint(@Body() pointData: TravelPoint): Promise<TravelPoint> {
    return await this._tpService.create(pointData);
  }

  @Delete(':id')
  async removePoint(@Param('id') id: string): Promise<{status: boolean}> {
    return await this._tpService.remove(id);
  }

  @Delete('/by-group/:groupName')
  async removePointsByGroupName(@Param('groupName') groupName: string): Promise<{status: boolean}> {
    return await this._tpService.removeByGroupName(groupName);
  }

  @Get(':id/routes')
  @UseFilters(new HttpExceptionFilter())
  async getTravelPointRoutes(@Param('id') id: string): Promise<any> {
    return await this._tpService.getTravelPointRoutes(id);
  }
}
