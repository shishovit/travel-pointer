import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule} from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';

import { TravelPointModule } from './travel-point/travel-point.module';
import { IncidentPointModule } from './incident-point/incident-point.module';

import { MindConnectApiService } from './mindconnect.service';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { TravelMatrixSchema } from './travel-matrix/travel-matrix.schema';
import { IncidentPointSchema } from './incident-point/incident-point.schema';
import { TravelPointSchema } from './travel-point/travel-point.schema';
@Module({
  imports: [
    TravelPointModule,
    IncidentPointModule,
    HttpModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb://localhost:27017/travel-point'),
    MongooseModule.forFeature([{name: 'TravelPoint', schema: TravelPointSchema}]),
    MongooseModule.forFeature([{name: 'IncidentPoint', schema: IncidentPointSchema}]),
    MongooseModule.forFeature([{name: 'TravelMatrix', schema: TravelMatrixSchema}]),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    MindConnectApiService,
  ],
})
export class AppModule {}
